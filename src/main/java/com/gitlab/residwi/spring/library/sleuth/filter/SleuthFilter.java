package com.gitlab.residwi.spring.library.sleuth.filter;

import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.cloud.sleuth.autoconfig.instrument.web.SleuthWebProperties;
import org.springframework.core.Ordered;

import javax.servlet.*;
import java.io.IOException;
import java.util.Objects;

public interface SleuthFilter extends Filter, Ordered {

    Tracer getTracer();

    @Override
    default int getOrder() {
        return SleuthWebProperties.TRACING_FILTER_ORDER + 10;
    }

    @Override
    default void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        var currentSpan = getCurrentSpan(servletRequest);

        if (Objects.nonNull(currentSpan)) {
            doFilter(servletRequest, servletResponse, filterChain, currentSpan);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    default Span getCurrentSpan(ServletRequest servletRequest) {
        var span = getTracer().currentSpan();
        if (Objects.isNull(span)) {
            span = (Span) servletRequest.getAttribute(Span.class.getName());
        }
        return span;
    }

    void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain, Span currentSpan) throws IOException, ServletException;
}

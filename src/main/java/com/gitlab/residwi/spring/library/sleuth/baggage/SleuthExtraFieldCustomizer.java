package com.gitlab.residwi.spring.library.sleuth.baggage;

import brave.baggage.BaggageField;
import brave.baggage.BaggagePropagation;
import brave.baggage.BaggagePropagationConfig;
import brave.baggage.BaggagePropagationCustomizer;
import com.gitlab.residwi.spring.library.sleuth.configuration.SleuthConfiguration;
import com.gitlab.residwi.spring.library.sleuth.fields.SleuthExtraFields;

import java.util.List;

public class SleuthExtraFieldCustomizer implements BaggagePropagationCustomizer {

    private final List<SleuthExtraFields> sleuthExtraFields;

    public SleuthExtraFieldCustomizer(List<SleuthExtraFields> sleuthExtraFields) {
        this.sleuthExtraFields = sleuthExtraFields;
    }

    @Override
    public void customize(BaggagePropagation.FactoryBuilder builder) {
        sleuthExtraFields.forEach(
                fields -> fields.getFields().forEach(
                        field -> builder.add(BaggagePropagationConfig.SingleBaggageField.newBuilder(BaggageField.create(field))
                                .addKeyName(SleuthConfiguration.HTTP_BAGGAGE_PREFIX + field) /* for HTTP */
                                .addKeyName(SleuthConfiguration.MESSAGING_BAGGAGE_PREFIX + field) /* for Messaging */
                                .build())));
    }
}

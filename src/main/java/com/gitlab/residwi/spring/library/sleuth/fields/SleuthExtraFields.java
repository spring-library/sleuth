package com.gitlab.residwi.spring.library.sleuth.fields;

import java.util.List;

public interface SleuthExtraFields {

    List<String> getFields();
}
